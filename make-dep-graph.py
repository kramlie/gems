#!/usr/bin/python3

# Example Jira link: https://tracker.mender.io/issues/?jql=issueFunction%20in%20linkedIssuesOfRecursive(%27%22Epic%20Link%22%20%3D%20MEN-2462%27%2C%20%22is%20blocked%20by%22)%20OR%20%22Epic%20Link%22%20%3D%20MEN-2462

import json
import os
import requests
import subprocess
import sys
import textwrap

# Return a string containing key=value.
def get_jira_auth():
    tmpfile = os.path.join(os.environ["HOME"], ".tmp-make-dep-graph.cookies.sqlite")
    subprocess.check_call(f"cp $HOME/.mozilla/firefox/*.default/cookies.sqlite {tmpfile}", shell=True)
    keys = subprocess.check_output(["sqlite3",
                                   tmpfile,
                                   "select name from moz_cookies where host = 'northerntech.atlassian.net' order by name",
    ]).decode().strip().split("\n")
    values = subprocess.check_output(["sqlite3",
                                     tmpfile,
                                     "select value from moz_cookies where host = 'northerntech.atlassian.net' order by name",
    ]).decode().strip().split("\n")
    os.unlink(tmpfile)
    assert len(keys) == len(values)
    ret = "; ".join([f"{keys[index]}={values[index]}" for index in range(0, len(keys))])
    return ret

def get_json(auth, ticket):
    headers = {
        "Cookie": auth,
        "Content-Type": "application/json",
    }
    # url = f"https://northerntech.atlassian.net/rest/api/latest/search?jql=issueFunction+in+linkedIssuesOfRecursive%28%27%22Epic+Link%22+%3D+{ticket}%27%2C+%22is+blocked+by%22%29+OR+%22Epic+Link%22+%3D+{ticket}"
    url = f"https://northerntech.atlassian.net/rest/api/latest/search?jql=Parent+%3D+{ticket}"
    response = requests.get(
        url,
        headers=headers,
    )

    data = json.loads(response.text)

    assert "issues" in data, f"auth: {auth}, response: {response.text}"

    # Recursively look for is-blocked-by linked tickets, since Jira Cloud no longer supports this
    # natively.
    looked_at = set()
    looked_at.add(ticket)
    for issue in data["issues"]:
        looked_at.add(issue["key"])
    issue_list = []
    def process_blocked_by_recursive(data):
        for issue in data["issues"]:
            if issue["fields"].get("issuelinks") is None:
                continue
            for link in issue["fields"]["issuelinks"]:
                if link["type"]["name"] != "Blocks" or link.get("inwardIssue") is None:
                    continue

                linked = link["inwardIssue"]["key"]
                if linked in looked_at:
                    continue

                looked_at.add(linked)
                url = f"https://northerntech.atlassian.net/rest/api/latest/search?jql=id+%3D+{linked}"
                response = requests.get(
                    url,
                    headers=headers,
                )
                new_data = json.loads(response.text)
                assert len(new_data["issues"]) == 1
                issue_list.append(new_data["issues"][0])

                # Actually not recursive ATM, one level is enough.
                # process_blocked_by_recursive(new_data)

    if not only_internal_deps:
        process_blocked_by_recursive(data)

    data["issues"] += issue_list

    return data

def render_pdf(main_epic, data, pdf=None):
    tickets = []
    deps = []

    dot = main_epic + ".dot"
    if pdf is None:
        pdf = main_epic + "-deps.pdf"

    try:
        for issue in data["issues"]:
            key = issue["key"]
            summary = issue["fields"]["summary"]
            if "internal for release reporting" in summary:
                continue
            summary = "\n".join(textwrap.wrap(summary, 40))
            summary = summary.replace('"', r'\"')
            status = issue["fields"]["status"]["name"]
            sp = issue["fields"].get("customfield_10036")
            if sp is None:
                sp = ""
            else:
                sp = int(sp)
            try:
                epic = issue["fields"]["parent"]["key"]
            except KeyError:
                epic = None

            tickets.append((key, summary, sp, status, epic))
            deps += [(key, target["outwardIssue"]["key"]) for target in issue["fields"]["issuelinks"]
                     if target["type"]["name"] == "Blocks" and target.get("outwardIssue") is not None]
    except KeyError as e:
        print(data)
        raise

    with open(dot, "w") as fd:
        fd.write("digraph DI {\n")
        # Use backwards references so that tasks gravitate towards the right. This makes the longest
        # chains more visible as things to attack first. This means the arrows in the rendering are
        # opposite of the arrows in here.
        fd.write("rankdir=RL\n")
        fd.write("edge [ dir=back ]\n")
        fd.write("concentrate=true\n")
        fd.write("rank=source\n")

        # fd.write('"Start" [shape=point]\n')
        # ticket_has_deps = {}
        for ticket in tickets:
            # ticket_has_deps[ticket[0]] = False
            color = ""
            shape = "box"
            end_node = False
            # Highlight tickets that don't have dependees (external dependees omitted).
            if ticket[0] not in [dep[0] for dep in deps if dep[1] in [ticket[0] for ticket in tickets]]:
                shape = "box3d"
                end_node = True
            if ticket[3] in ["In Progress", "In Review"]:
                color += " style=filled fillcolor=yellow"
            if ticket[3] in ["Done", "Rejected"]:
                color += " style=filled fillcolor=grey80"
            if ticket[4] is not None and ticket[4] != "" and ticket[4] != main_epic:
                color += " color=grey fontcolor=grey40"
                shape = "ellipse"
            fd.write('"%s" [shape=%s %s label="%s (%s SP)\n%s" URL="https://northerntech.atlassian.net/browse/%s"]\n' % (ticket[0], shape, color, ticket[0], ticket[2], ticket[1], ticket[0]))

        for dep in deps:
            if dep[1] in [ticket[0] for ticket in tickets]:
                # Attention: Opposite arrow direction of what is rendered in the pdf. See comment
                # about rankdir above.
                fd.write('"%s" -> "%s"\n' % (dep[1], dep[0]))

        fd.write("}\n")

    subprocess.check_call(["dot", "-Tpdf", dot, "-o", pdf])
    os.remove(dot)

    subprocess.check_call(["xdg-open", pdf])

    return pdf

def upload(auth, ticket, file):
    headers = {
        "Cookie": auth,
        "Content-Type": "application/json",
    }
    url = f"https://northerntech.atlassian.net/rest/api/latest/issue/{ticket}"
    response = requests.get(
        url,
        headers=headers,
    )

    data = json.loads(response.text)

    assert "fields" in data, response.text
    assert "attachment" in data["fields"], response.text

    for attachment in data["fields"]["attachment"]:
        if attachment["filename"] == file:
            id = attachment["id"]
            url = f"https://northerntech.atlassian.net/rest/api/latest/attachment/{id}"
            response = requests.delete(
                url,
                headers=headers,
            )

    headers["X-Atlassian-Token"] = "no-check"
    url = f"https://northerntech.atlassian.net/rest/api/latest/issue/{ticket}/attachments"
    del headers["Content-Type"]
    response = requests.post(
        url,
        headers=headers,
        files = {
            "file": (os.path.basename(file), open(file, "rb"), "application/pdf")
        }
    )

    assert response.status_code == 200 or response.status_code == 201, f"{response.status_code}: {response.text}"

def usage():
    cmd = os.path.basename(sys.argv[0])
    print("""Supported commands are:

%s [OPTIONS] {ticket-number} [output-file]
\tRender PDF based on CSV input.
\tDefaults to rendering a .pdf file in the same folder as the input.

Options:

-i, --only-internal-deps
\tDon't render dependencies that are external to the epic.

--upload
\tAutomatically upload the PDF to the ticket, deleting the existing one
\tif it exists.""" % (cmd))

bare_arg_num = 0
upload_flag = False
only_internal_deps = False
ticket = None
output = None
for arg in sys.argv[1:]:
    if arg == "--upload":
        upload_flag = True
    elif arg == "-i" or arg == "--only-internal-deps":
        only_internal_deps = True
    elif arg == "-h" or arg == "--help":
        usage()
        sys.exit(0)
    elif bare_arg_num == 0:
        ticket = arg
        bare_arg_num += 1
    elif bare_arg_num == 1:
        output = arg
        bare_arg_num += 1
    else:
        print(f"Unrecognized argument: {arg}")
        sys.exit(1)

if not ticket:
    usage()
    sys.exit(1)

auth = get_jira_auth()
data = get_json(auth, ticket)
pdf = render_pdf(ticket, data, output)

if upload_flag:
    upload(auth, ticket, pdf)
